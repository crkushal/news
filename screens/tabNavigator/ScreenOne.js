import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loadNewsData } from '../../actions/actionCreators';
import NewsHomeScreen from './NewsHomeScreen';
import { createStackNavigator } from 'react-navigation';
import SingleNews from './SingleNews';

class ScreenOne extends Component {
   // static navigationOptions = {
   //    header: 'Home'
   // }
   constructor(props) {
      super(props);
      this.props.loadNewsData();
   }
   render() {
      return (
         <StackNavigator />
      );
   }
}

const StackNavigator = new createStackNavigator({
   NewsHomeScreen: { screen: NewsHomeScreen },
   SingleNews: { screen: SingleNews },
});

export default connect(null, { loadNewsData })(ScreenOne);
