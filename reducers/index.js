import { combineReducers } from 'redux';
import { loadNewsReducer } from './loadNewsReducer';
import { loadSportNewsReducer } from './loadNewsReducer';
import { loadCnnNewsReducer } from './loadNewsReducer';

const rootReducer = combineReducers({
   data: loadNewsReducer,
   sportNews: loadSportNewsReducer,
   cnnNews: loadCnnNewsReducer,
});

export default rootReducer;