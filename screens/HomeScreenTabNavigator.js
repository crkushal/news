import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';
import ScreenOne from './tabNavigator/ScreenOne';
import ScreenTwo from './tabNavigator/ScreenTwo';
import { Ionicons } from '@expo/vector-icons';

export default class AppTabNavigator extends Component {

   static navigationOptions = ({ navigation }) => {
      return {
         headerLeft: (
            <View>
               <Ionicons name="ios-menu-outline" style={{ paddingLeft: 10, }} size={24} />
            </View>
         )
      }
   }
   render() {
      return (
         <HomeScreenTabNavigator screenProps={{ navigation: this.props.navigation }} />
      );
   }
}

const HomeScreenTabNavigator = new createMaterialTopTabNavigator({
   ScreenOne: {
      screen: ScreenOne,
      navigationOptions: {
         tabBarLabel: 'Home',
         tabBarIcon: ({ tintColor }) => (
            <Ionicons color={tintColor} name="ios-home-outline" size={24} />
         )
      }
   },
   ScreenTwo: {
      screen: ScreenTwo,
      navigationOptions: {
         tabBarLabel: 'Categories',
         tabBarIcon: ({ tintColor }) => (
            <Ionicons color={tintColor} name="ios-settings-outline" size={24} />
         )
      }
   }
}, {
      tabBarPosition: 'bottom',
      navigationOptions: {
         swipeEnabled: true,
         animationEnabled: true,
      },
      tabBarOptions: {
         showIcon: true,
         activeTintColor: 'blue',
         inactiveTintColor: 'grey',
         pressColor: 'blue',
         style: {
            backgroundColor: 'white',
         },
      }
   });