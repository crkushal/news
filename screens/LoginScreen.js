import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

class LoginScreen extends Component {

   renderLoginView = () => {
      return (
         <View style={styles.container}>
            <Button
               title="way to home Screen"
               onPress={
                  () => this.props.navigation.navigate('AppDrawerNavigator')
               }
            />
         </View>
      )
   }
   render = () => this.renderLoginView();
}
const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   }
});
export default LoginScreen;
