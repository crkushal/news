import React, { Component } from 'react';
import { View, StyleSheet, Button } from 'react-native';

class HomeScreen extends Component {
   renderLoginView = () => {
      return (
         <View style={styles.container}>
            <Button
               title="HomeScreen"
               onPress={
                  () => this.props.navigation.navigate('LoginScreen')
               }
            />
         </View>
      )
   }
   render = () => this.renderLoginView();
}
const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   }
});
export default HomeScreen;
