import LoadNewsApi from '../class/LoadNewsApi';
let newsUrl = `https://newsapi.org/v2/top-headlines?sources=daily-mail&apiKey=4e9f5d53e00e4992999ec071f7b5a867`;
let sportNewsUrl = `https://newsapi.org/v2/top-headlines?sources=espn&apiKey=4e9f5d53e00e4992999ec071f7b5a867`;
let cnnNewsUrl = `https://newsapi.org/v2/top-headlines?sources=cnn&apiKey=4e9f5d53e00e4992999ec071f7b5a867`;

export const loadNewsData = () => {
	return function (dispatch) {
		return LoadNewsApi.getNews(newsUrl)
			.then(news => dispatch(loadNewsSuccess(news)))
			.catch(error => { throw (error) });
	}
}

export const loadSportNews = () => {
	return function (dispatch) {
		return LoadNewsApi.getNews(sportNewsUrl)
			.then(news => dispatch(loadSportNewsOnSuccess(news)))
			.catch(error => { throw (error) });
	}
}

export const loadCnnNews = () => {
	return function (dispatch) {
		return LoadNewsApi.getNews(cnnNewsUrl)
			.then(news => dispatch(loadCnnNewsOnSuccess(news)))
			.catch(error => { throw (error) });
	}
}


// helper functions
const loadCnnNewsOnSuccess = (news) => {
	return {
		type: 'LOAD_CNN_NEWS_ON_SUCCESS',
		news
	}
};

const loadNewsSuccess = (news) => {
	return {
		type: 'LOAD_NEWS_ON_SUCCESS',
		news
	}
};

const loadSportNewsOnSuccess = (news) => {
	return {
		type: 'LOAD_SPORT_NEWS_ON_SUCCESS',
		news
	}
}