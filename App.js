import React from 'react';
import store from './store';
import { Provider } from 'react-redux';
import AppStackNavigator from './navigator/AppStackNavigator';

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppStackNavigator />
      </Provider>
    )
  };
}


