import { createStackNavigator } from 'react-navigation';
import WelcomeScreen from '../screens/WelcomeScreen';
import LoginScreen from '../screens/LoginScreen';
import AppDrawerNavigator from '../screens/DrawerNavigator';

const AppStackNavigator = new createStackNavigator({
   WelcomeScreen: { screen: WelcomeScreen },
   LoginScreen: { screen: LoginScreen },
   AppDrawerNavigator: {
      screen: AppDrawerNavigator,
      navigationOptions: {
         header: null,
      }
   }
});
export default AppStackNavigator;