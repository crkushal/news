class LoadNewsApi {
	static getNews(url) {
		let apiUrl = url;
		return fetch(apiUrl)
			.then(response => response.json())
			.catch(error => error);
	}
}
export default LoadNewsApi;