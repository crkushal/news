const initialState = [];
export const loadNewsReducer = (state = initialState, action) => {
	switch (action.type) {
		case "LOAD_NEWS_ON_SUCCESS":
			return action.news;
		default:
			return state;
	}
}

export const loadSportNewsReducer = (state = initialState, action) => {
	switch (action.type) {
		case "LOAD_SPORT_NEWS_ON_SUCCESS":
			return action.news;
		default:
			return state;
	}
}
export const loadCnnNewsReducer = (state = initialState, action) => {
	switch (action.type) {
		case "LOAD_CNN_NEWS_ON_SUCCESS":
			return action.news;
		default:
			return state;
	}
}