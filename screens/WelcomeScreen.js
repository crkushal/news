import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

class WelcomeScreen extends Component {

   static navigationOptions = {
      header: null,
   }
   renderLoginView = () => {
      return (
         <View style={styles.container}>
            <Button
               title="login"
               onPress={
                  () => this.props.navigation.navigate('LoginScreen')
               }
            />
         </View>
      )
   }
   render = () => this.renderLoginView();
}
const styles = StyleSheet.create({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   }
});
export default WelcomeScreen;
