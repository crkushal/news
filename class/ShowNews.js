class ShowNews {
   constructor() {
      this.news = this.renderNews(rNews)
   }

   renderNews = (rNews) => {
      return (
         <ScrollView horizontal={true}>
            <FlatList
               horizontal
               data={rNews.articles}
               renderItem={({ item, index }) => {
                  return (
                     <View style={styles.card}>
                        <TouchableOpacity>
                           <View style={styles.cardContent}>
                              <Image
                                 style={styles.image}
                                 source={{ uri: item.urlToImage }}
                              />
                              <Text numberOfLines={3} style={styles.text}>
                                 {item.title}
                              </Text>
                           </View>
                        </TouchableOpacity>
                     </View>
                  )
               }}
            />
         </ScrollView>
      );
   }
   displayNews = () => { return (this.news) }
}
export default ShowNews;