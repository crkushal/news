import React, { Component } from 'react';
import { createDrawerNavigator, createStackNavigator, DrawerItems } from 'react-navigation';
import HomeScreenTabNavigator from './HomeScreenTabNavigator';
import { View, StyleSheet, Image } from 'react-native';

const InnerStackNavigator = new createStackNavigator({
	TabNavigator: {
		screen: HomeScreenTabNavigator,
	}
});

const customDrawerContentComponent = (props) => {
	return (
		<View>
			<View style={styles.container}>
				<Image
					style={styles.drawerimage}
					source={require('../assets/real.png')}
				/>
				<DrawerItems {...props} />
			</View>
		</View>
	);
}
const AppDrawerNavigator = new createDrawerNavigator({
	HomeScreen: { screen: InnerStackNavigator },
	SettinScreen: { screen: InnerStackNavigator }
}, {
		contentComponent: customDrawerContentComponent,
		drawerOpenRoute: 'DrawerOpen',
		drawerCloseRoute: 'DrawerClose',
		drawerToggleRoute: 'DrawerToggle',
	});

const styles = StyleSheet.create({
	container: {
		paddingHorizontal: 30,
		paddingVertical: 15,
	},
	drawerimage: {
		width: 150,
		height: 150,
		borderRadius: 75,
	}
})

export default AppDrawerNavigator;
