import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';
import { loadSportNews } from '../../actions/actionCreators';
import { loadCnnNews } from '../../actions/actionCreators';
import { connect } from 'react-redux';
import SingleNews from './SingleNews';
import NewsCategories from './NewsCategories';

class ScreenTwo extends Component {
   constructor(props) {
      super(props);
      this.props.loadSportNews();
      this.props.loadCnnNews();
   }
   renderLoginView = () => {
      return (
         <SecondStackNavigator />
      )
   }
   render = () => this.renderLoginView();
}
const SecondStackNavigator = new createStackNavigator({
   NewsCategoriesScreen: {
      screen: NewsCategories,
      navigationOptions: {
         header: null,
      }
   },
   SingleNews: { screen: SingleNews }
})
export default connect(null, { loadSportNews, loadCnnNews })(ScreenTwo);
