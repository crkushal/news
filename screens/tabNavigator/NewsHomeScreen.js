import React, { Component } from 'react';
import { View, ActivityIndicator, Image, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';

class NewsHomeScreen extends Component {
   static navigationOptions = {
      header: null,
   }
   renderNewsView = () => {
      const { news } = this.props;
      if (news.length !== 0) {
         return (
            <FlatList
               data={news.articles}
               renderItem={({ item, index }) => {
                  return (
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleNews', { data: item })}>
                        <View style={styles.card}>
                           <Image
                              style={styles.image}
                              source={{ uri: item.urlToImage }}
                           />
                           <Text style={styles.headline}>{item.title}</Text>
                        </View>
                     </TouchableOpacity>
                  )
               }}
            />
         );
      }
      else {
         return (
            <View style={styles.activity}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
         );
      }
   }
   render() {
      return (
         <View style={styles.container}>
            {this.renderNewsView()}
         </View>
      );
   }
}
const mapStateToProps = (state) => {
   return {
      news: state.data,
   }
}
const styles = StyleSheet.create({
   container: {
      flex: 1,
   },
   activity: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   },
   card: {
      backgroundColor: 'white',
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginHorizontal: 10,
      marginVertical: 10,
   },
   image: {
      width: null,
      height: 150,
      alignItems: 'stretch',
   },
   headline: {
      color: 'black',
      fontSize: 18,
      paddingHorizontal: 5,
      paddingVertical: 5,
   }

});
export default connect(mapStateToProps)(NewsHomeScreen);
