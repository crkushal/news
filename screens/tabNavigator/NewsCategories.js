import React, { Component } from 'react';
import { View, TouchableOpacity, FlatList, Text, StyleSheet, ScrollView, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

class NewsCategories extends Component {
   renderNews = (rNews) => {
      return (
         <ScrollView horizontal={true}>
            <FlatList
               horizontal
               data={rNews.articles}
               renderItem={({ item, index }) => {
                  return (
                     <View style={styles.card}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('SingleNews', { data: item })}>
                           <View style={styles.cardContent}>
                              <Image
                                 style={styles.image}
                                 source={{ uri: item.urlToImage }}
                              />
                              <Text numberOfLines={3} style={styles.text}>
                                 {item.title}
                              </Text>
                           </View>
                        </TouchableOpacity>
                     </View>
                  )
               }}
            />
         </ScrollView>
      );
   }
   render() {
      const rNews = this.props.rNews;
      const cNews = this.props.cNews;
      const sNews = this.props.sNews;
      if (rNews.length !== 0 && sNews.length !== 0 && cNews.length !== 0) {
         return (
            <ScrollView>
               <View>
                  <Text style={styles.heading}>BBBC</Text>
                  {this.renderNews(rNews)}
               </View>
               <View>
                  <Text style={styles.heading}>CNN</Text>
                  {this.renderNews(cNews)}
               </View>
               <View>
                  <Text style={styles.heading}>ESPN</Text>
                  {this.renderNews(sNews)}
               </View>
            </ScrollView>
         );
      }
      else {
         return (
            <View style={styles.activity}>
               <ActivityIndicator size="large" color="#0000ff" />
            </View>
         );
      }

   }
}
const mapStateToProps = (state) => {
   return {
      sNews: state.sportNews,
      rNews: state.data,
      cNews: state.cnnNews
   }
}
const styles = StyleSheet.create({
   container: {
      flex: 1,
   },
   heading: {
      marginLeft: 10,
      marginTop: 10,
      fontSize: 18
   },
   card: {
      backgroundColor: 'white',
      marginVertical: 5,
   },
   text: {
      width: 100,
      fontSize: 16
   },
   cardContent: {
      paddingHorizontal: 10,
      paddingVertical: 15,
   },
   image: {
      width: 100,
      height: 100,
   },
   activity: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
   }
});
export default connect(mapStateToProps)(NewsCategories);
