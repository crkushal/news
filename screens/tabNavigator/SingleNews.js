import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

class SingleNews extends Component {
   static navigationOptions = {
      header: null,
   }
   render() {
      const newsData = this.props.navigation.state.params.data;
      return (
         <View style={styles.container}>
            <View style={styles.card}>
               <Image
                  source={{ uri: newsData.urlToImage }}
                  style={styles.image}
               />
               <Text style={styles.headline}>
                  {newsData.description}
               </Text>
            </View>
         </View>
      );
   }
}
const styles = StyleSheet.create({
   container: {
      flex: 1,
   },
   card: {
      backgroundColor: 'white',
      paddingHorizontal: 20,
      paddingVertical: 10,
      marginHorizontal: 10,
      marginVertical: 10,
   },
   image: {
      width: null,
      height: 150,
      alignItems: 'stretch',
      marginTop: 15,
   },
   headline: {
      color: 'black',
      fontSize: 18,
      paddingHorizontal: 5,
      paddingVertical: 10,
   }

});
export default SingleNews;
